import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Home from '../views/Home';
import Profile from '../views/Profile';
import NoteDetails from '../views/NoteDetails';

export type StackParamList = {
  Home: undefined;
  Profile: { userId: string };
  NoteDetails: { noteId: string };
}

const Stack = createStackNavigator<StackParamList>()

const Routes = () => {
  return (
    <Stack.Navigator initialRouteName="Home">
      <Stack.Screen
        name="Home"
        component={Home}
        options={{ title: 'Awsome Notes' }}
      />
      <Stack.Screen name="Profile" component={Profile} />
      <Stack.Screen name="NoteDetails" component={NoteDetails} />
    </Stack.Navigator>
  )
}

export default Routes
