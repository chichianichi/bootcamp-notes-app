import React from 'react';
import {StackNavigationProp} from '@react-navigation/stack';
import {StackParamList} from '../config/routes';
import Note from '../models/note.model';
import {
  Button,
  FlatList,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  TextInput,
} from 'react-native';
import {addNote, deleteNote} from '../store/actions';
import {useAppSelector, useAppDispatch} from '../hooks';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

type HomeNaviationProp = StackNavigationProp<StackParamList, 'Home'>;
type HomeProps = {
  navigation: HomeNaviationProp;
};

const Home = ({navigation}: HomeProps) => {
  const notes = useAppSelector(state => state.notes);
  const dispatch = useAppDispatch();

  let newTitle = '';
  let newContent = '';

  const updateTitle = (txt: string) => {
    if (txt == '') {
      return;
    }
    newTitle = txt;
  };

  const updateContent = (txt: string) => {
    if (txt == '') {
      return;
    }
    newContent = txt;
  };

  const addNewNote = () => {
    console.log(newTitle, newContent);
    if (newTitle != '' || newContent != '') {
    }
    const newId = notes.length + 1;
    const newNote: Note = {
      id: `${newId}`,
      title: `${newTitle}`,
      content: `${newContent}`,
      date: new Date().toDateString(),
    };
    dispatch(addNote(newNote));
  };

  const delNote = (noteId: string) => () => {
    console.log('delNote function');
    dispatch(deleteNote(noteId));
  };

  const handleItemClick = (noteId: string) => () => {
    navigation.navigate('NoteDetails', {noteId: noteId});
  };

  return (
    <KeyboardAwareScrollView>
      <View style={styles.container}>
        <Button
          title="Go to Profile"
          onPress={() => navigation.navigate('Profile', {userId: 'Jona'})}
        />
        <FlatList
          style={styles.listItem}
          data={notes}
          renderItem={({item}) => (
            <TouchableOpacity
              style={styles.noteList}
              onPress={handleItemClick(item.id)}>
              <Text style={styles.item}>{item.title}</Text>
              <Button title="Delete" onPress={delNote(item.id)} />
            </TouchableOpacity>
          )}
        />

        <View style={styles.formArea}>
          <Text>NEW TITLE:</Text>
          <TextInput
            style={styles.inputArea}
            clearButtonMode="always"
            onChangeText={txt => {
              updateTitle(txt);
            }}
            placeholder={'Keep the input cleaned before adding notes'}
          />
          <Text>NEW CONTENT:</Text>
          <TextInput
            style={styles.inputArea}
            placeholder={'Keep the input cleaned before adding notes'}
            onChangeText={txt => {
              updateContent(txt);
            }}
          />
          <Button title="Save Change" onPress={addNewNote} />
        </View>
      </View>
    </KeyboardAwareScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 32,
    paddingHorizontal: 24,
  },
  item: {
    padding: 10,
    height: 44,
    fontSize: 18,
  },
  listItem: {
    display: 'flex',
  },
  noteList: {
    marginTop: 32,
    paddingHorizontal: 24,
    backgroundColor: '#AFEEEE',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  formArea: {
    flex: 1,
    marginTop: 32,
    paddingHorizontal: 24,
    backgroundColor: '#87CEFA',
    marginBottom: 30,
  },
  inputArea: {
    backgroundColor: '#AFEEEE',
  },
});

export default Home;
