import React from 'react';
import { StackParamList } from '../config/routes';
import { StackScreenProps } from '@react-navigation/stack';
import { Text } from 'react-native';

type ProfileProps = StackScreenProps<StackParamList, 'Profile'>
const Profile = ({ navigation, route }: ProfileProps) => {
  return <Text>Hola {route.params.userId}</Text>
}

export default Profile
