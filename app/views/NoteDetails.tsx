import React from 'react';
import {StackParamList} from '../config/routes';
import {StackScreenProps} from '@react-navigation/stack';
import {Text, View, Button, TextInput, StyleSheet} from 'react-native';
import {useAppSelector, useAppDispatch} from '../hooks';
import Note from '../models/note.model';
import {updateNote} from '../store/actions';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

type NoteDetailsProps = StackScreenProps<StackParamList, 'NoteDetails'>;
const NoteDetails = ({route}: NoteDetailsProps) => {
  const notes = useAppSelector(state => state.notes);
  const dispatch = useAppDispatch();

  const noteId = route.params.noteId;
  const selectedNote = notes.find(n => n.id === noteId);

  let newTitle = selectedNote?.title;
  let newContent = selectedNote?.content;
  let newDate = selectedNote?.date;

  const updateTitle = (txt: string) => {
    if (txt == newTitle) {
      return;
    }
    newTitle = txt;
  };

  const updateContent = (txt: string) => {
    if (txt == newContent) {
      return;
    }
    newContent = txt;
  };

  const updateDate = (txt: string) => {
    if (txt == newDate) {
      return;
    }
    newDate = txt;
  };

  const updateCurrentNote = () => {
    if (
      newTitle != selectedNote?.title ||
      newContent != selectedNote?.content ||
      newDate != selectedNote?.date
    ) {
      var noteChange: Note = {
        id: noteId,
        title: `${newTitle}`,
        content: `${newContent}`,
        date: `${newDate}`,
      };
      dispatch(updateNote(noteChange));
    }
  };

  return (
    <KeyboardAwareScrollView style={styles.container}>
      <Text style={styles.bigText}>Note details of: {noteId}</Text>
      <Text style={styles.bigText}>TITLE: {selectedNote?.title}</Text>
      <Text style={styles.bigText}>CONTENT: {selectedNote?.content}</Text>
      <Text style={styles.bigText}>DATE: {selectedNote?.date}</Text>

      <View style={styles.formArea}>
        <Text>NEW TITLE:</Text>
        <TextInput
          style={styles.inputArea}
          onChangeText={txt => updateTitle(txt)}
          defaultValue={''}
        />
        <Text>NEW CONTENT:</Text>
        <TextInput
          style={styles.inputArea}
          onChangeText={txt => updateContent(txt)}
          defaultValue={''}
        />
        <Text>NEW DATE:</Text>
        <TextInput
          style={styles.inputArea}
          onChangeText={txt => updateDate(txt)}
          defaultValue={''}
        />
        <Button title="Save Change" onPress={updateCurrentNote} />
      </View>
    </KeyboardAwareScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 32,
    paddingHorizontal: 24,
    marginVertical: 30,
  },
  formArea: {
    flex: 1,
    marginTop: 32,
    paddingHorizontal: 24,
    backgroundColor: '#87CEFA',
    marginBottom: 30,
  },
  inputArea: {
    backgroundColor: '#AFEEEE',
  },
  bigText: {
    fontSize: 33,
  },
});

export default NoteDetails;
