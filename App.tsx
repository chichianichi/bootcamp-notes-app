import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import Routes from './app/config/routes';
import { Provider } from 'react-redux';
import store from './app/config/store';

const App = () => {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <Routes />
      </NavigationContainer>
    </Provider>
  );
};

export default App;
